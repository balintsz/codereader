﻿
namespace CodeReader
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbScanned = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbInventoryPath = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbBarcode = new System.Windows.Forms.ComboBox();
            this.cbAmount = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbSeparator = new System.Windows.Forms.Label();
            this.bAddProduct = new System.Windows.Forms.Button();
            this.bStartScanning = new System.Windows.Forms.Button();
            this.bExit = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 224);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Codul scanat";
            // 
            // tbScanned
            // 
            this.tbScanned.Location = new System.Drawing.Point(98, 221);
            this.tbScanned.Name = "tbScanned";
            this.tbScanned.ReadOnly = true;
            this.tbScanned.Size = new System.Drawing.Size(100, 20);
            this.tbScanned.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Step 1: Cauta tabelul cu inventar";
            // 
            // tbInventoryPath
            // 
            this.tbInventoryPath.Location = new System.Drawing.Point(15, 25);
            this.tbInventoryPath.Name = "tbInventoryPath";
            this.tbInventoryPath.Size = new System.Drawing.Size(437, 20);
            this.tbInventoryPath.TabIndex = 7;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(458, 23);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 8;
            this.btnBrowse.Text = "Cautare";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(239, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Step 2: Selecteaza coloana pentru codul de bare";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(194, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Step 3: Selecteaza coloana pentru stoc";
            // 
            // cbBarcode
            // 
            this.cbBarcode.FormattingEnabled = true;
            this.cbBarcode.Location = new System.Drawing.Point(15, 69);
            this.cbBarcode.Name = "cbBarcode";
            this.cbBarcode.Size = new System.Drawing.Size(121, 21);
            this.cbBarcode.TabIndex = 12;
            this.cbBarcode.SelectedIndexChanged += new System.EventHandler(this.cbBarcode_SelectedIndexChanged);
            // 
            // cbAmount
            // 
            this.cbAmount.FormattingEnabled = true;
            this.cbAmount.Location = new System.Drawing.Point(15, 111);
            this.cbAmount.Name = "cbAmount";
            this.cbAmount.Size = new System.Drawing.Size(123, 21);
            this.cbAmount.TabIndex = 13;
            this.cbAmount.SelectedIndexChanged += new System.EventHandler(this.cbAmount_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Step 4: ";
            // 
            // lbSeparator
            // 
            this.lbSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbSeparator.Location = new System.Drawing.Point(15, 247);
            this.lbSeparator.Name = "lbSeparator";
            this.lbSeparator.Size = new System.Drawing.Size(542, 10);
            this.lbSeparator.TabIndex = 15;
            // 
            // bAddProduct
            // 
            this.bAddProduct.Enabled = false;
            this.bAddProduct.Location = new System.Drawing.Point(431, 214);
            this.bAddProduct.Name = "bAddProduct";
            this.bAddProduct.Size = new System.Drawing.Size(102, 23);
            this.bAddProduct.TabIndex = 16;
            this.bAddProduct.Text = "Adauga produs nou";
            this.bAddProduct.UseVisualStyleBackColor = true;
            this.bAddProduct.Click += new System.EventHandler(this.bAddProduct_Click);
            // 
            // bStartScanning
            // 
            this.bStartScanning.Location = new System.Drawing.Point(62, 135);
            this.bStartScanning.Name = "bStartScanning";
            this.bStartScanning.Size = new System.Drawing.Size(100, 23);
            this.bStartScanning.TabIndex = 17;
            this.bStartScanning.Text = "Incepe scanarea";
            this.bStartScanning.UseVisualStyleBackColor = true;
            this.bStartScanning.Click += new System.EventHandler(this.bStartScanning_Click);
            // 
            // bExit
            // 
            this.bExit.Location = new System.Drawing.Point(258, 190);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(75, 23);
            this.bExit.TabIndex = 18;
            this.bExit.Text = "Inchide";
            this.bExit.UseVisualStyleBackColor = true;
            this.bExit.Click += new System.EventHandler(this.bExit_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 167);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(321, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Step 5: In timpul scanarii fereastra neagra trebuie sa fie in prim plan";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 195);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(240, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Step 6: Apasa Inchide cand ai terminat de scanat";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 669);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bExit);
            this.Controls.Add(this.bStartScanning);
            this.Controls.Add(this.bAddProduct);
            this.Controls.Add(this.lbSeparator);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbAmount);
            this.Controls.Add(this.cbBarcode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.tbInventoryPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbScanned);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "CodeReader+1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbScanned;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbInventoryPath;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbBarcode;
        private System.Windows.Forms.ComboBox cbAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbSeparator;
        private System.Windows.Forms.Button bAddProduct;
        private System.Windows.Forms.Button bStartScanning;
        private System.Windows.Forms.Button bExit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}

