﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Management;

namespace CodeReader
{
    public class BarcodeManager
    { 
        public static SerialPort GetPort()
        {
            return new SerialPort(GetBarcodeScannerName(), 9600, Parity.None, 8, StopBits.One);
        }
        public static List<string> GetComPortsName()
        {
            var lst = new List<string>();
            var ports = COMPortInfo.GetCOMPortsInfo();
            foreach (var port in ports)
            {
                lst.Add(port.Description);
            }
            return lst;
        }
        private static string GetBarcodeScannerName()
        {
            var ports = COMPortInfo.GetCOMPortsInfo();
            foreach (var port in ports)
            {
                if(port.Description.Contains("Barcode Scanner"))
                {
                    return port.Name;
                }
            }
            throw new Exception("Barcode Scanner not found");
        }
        private static void PrintPorts()
        {
            var ports = COMPortInfo.GetCOMPortsInfo();
            foreach(var port in ports)
            {
                Console.WriteLine(port.ToString());
            }
        }
    }
    internal class ProcessConnection
    {
        // https://dariosantarelli.wordpress.com/2010/10/18/c-how-to-programmatically-find-a-com-port-by-friendly-name/
        public static ConnectionOptions ProcessConnectionOptions()
        {
            ConnectionOptions options = new ConnectionOptions();
            options.Impersonation = ImpersonationLevel.Impersonate;
            options.Authentication = AuthenticationLevel.Default;
            options.EnablePrivileges = true;
            return options;
        }

        public static ManagementScope ConnectionScope(string machineName, ConnectionOptions options, string path)
        {
            ManagementScope connectScope = new ManagementScope();
            connectScope.Path = new ManagementPath(@"\\" + machineName + path);
            connectScope.Options = options;
            connectScope.Connect();
            return connectScope;
        }

    }

    public class COMPortInfo
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public COMPortInfo() { }

        override
        public string ToString() { return Name + " " + Description; }

        public static List<COMPortInfo> GetCOMPortsInfo()
        {
            List<COMPortInfo> comPortInfoList = new List<COMPortInfo>();
            ConnectionOptions options = ProcessConnection.ProcessConnectionOptions();
            ManagementScope connectionScope = ProcessConnection.ConnectionScope(Environment.MachineName, options, @"\root\CIMV2");

            ObjectQuery objectQuery = new ObjectQuery("SELECT * FROM Win32_PnPEntity WHERE ConfigManagerErrorCode = 0");
            ManagementObjectSearcher comPortSearcher = new ManagementObjectSearcher(connectionScope, objectQuery);
            using (comPortSearcher)
            {
                string caption = null;
                foreach (ManagementObject obj in comPortSearcher.Get())
                {
                    if (obj != null)
                    {
                        object captionObj = obj["Caption"];

                        if (captionObj != null)
                        {
                            caption = captionObj.ToString();
                            if (caption.Contains("(COM"))
                            {
                                COMPortInfo comPortInfo = new COMPortInfo();
                                comPortInfo.Name = caption.Substring(caption.LastIndexOf("(COM")).Replace("(", string.Empty).Replace(")",
                                                                     string.Empty);
                                comPortInfo.Description = caption;
                                comPortInfoList.Add(comPortInfo);
                            }
                        }
                    }
                }
            }
            return comPortInfoList;
        }
    }
}
