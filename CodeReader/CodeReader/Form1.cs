﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Media;
using System.Threading;
using System.Runtime.InteropServices;

namespace CodeReader
{
    public partial class Form1 : Form
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool FreeConsole();

        private SerialPort port = null;
        public Microsoft.Office.Interop.Excel.Application application = new Microsoft.Office.Interop.Excel.Application();
        private Microsoft.Office.Interop.Excel.Workbook workbook = null;
        private Microsoft.Office.Interop.Excel.Worksheet worksheet = null;
        private Microsoft.Office.Interop.Excel.Range usedRange = null;
        private int codeColumnNumber = 0;
        private int amountColumnNumber = 0;
        private List<Control> dynamicLabels = new List<Control>();
        private List<Control> dynamicTextBoxes = new List<Control>();
        Thread thread = null;
        public Form1()
        {
            InitializeComponent();
            FormClosing += Form1_FormClosing;
            // switching to reading stdin method
            //try
            //{
            //    var lst = BarcodeManager.GetComPortsName();
            //    MessageBox.Show(String.Join("\n", lst));
            //    port = BarcodeManager.GetPort();
            //    ReadSerialPort();
            //}
            //catch(Exception e)
            //{
            //    MessageBox.Show(e.Message);
            //    System.Windows.Forms.Application.Exit();
            //    this.Close();
            //}
        }
        
        #region Utils
        private void StartListeningForStdin()
        {
            thread = new Thread(ListenForStdinBarcode);
            thread.Start();
        }
        private void ListenForStdinBarcode()
        {
            AllocConsole();
            try
            {
                while (true)
                {
                    var line = Console.ReadLine();
                    if (line != null && line != "")
                    {
                        ProcessBarcode(line);
                    }
                }
            }
            catch (ThreadAbortException abortException)
            {
                FreeConsole();
            }
            FreeConsole();
        }
        private void ReadSerialPort()
        {
            port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
            port.Open();
        }

        private void InitInventory()
        {
            workbook = application.Workbooks.Open(tbInventoryPath.Text);
            worksheet = workbook.ActiveSheet;
            usedRange = worksheet.UsedRange;
        }

        private void PopulateListboxes()
        {
            for(int j = 1; j <= usedRange.Columns.Count; j++)
            {
                if(worksheet.Cells[1, j].Value == null)
                {
                    return;
                }
                string content = worksheet.Cells[1, j].Value.ToString();
                cbBarcode.Items.Add(content);
                cbAmount.Items.Add(content);
            }
        }

        private void InitializeDynamicFields()
        {
            var currentX = lbSeparator.Location.X;
            var currentY = lbSeparator.Location.Y + 20;
            var labelWidth = 100;
            var textBoxWidth = 400;
            var labelComboBoxDistance = 10;
            this.SuspendLayout();
            for(int column = 1; column <= usedRange.Columns.Count; column++)
            {
                if(worksheet.Cells[1, column].Value == null)
                {
                    break;
                }
                var columnName = worksheet.Cells[1, column].Value.ToString();
                var label = new System.Windows.Forms.Label();
                label.AutoSize = false;
                label.Location = new System.Drawing.Point(currentX, currentY);
                label.Name = "lb_" + columnName;
                label.Size = new System.Drawing.Size(labelWidth, 20);
                label.TabIndex = 0;
                label.Text = columnName + ":";
                label.Show();
                this.Controls.Add(label);
                dynamicLabels.Add(label);

                var textBox = new System.Windows.Forms.TextBox();
                textBox.Location = new System.Drawing.Point(currentX + labelWidth + labelComboBoxDistance, currentY);
                textBox.Name = "tb_" + columnName;
                textBox.Size = new System.Drawing.Size(textBoxWidth, 20);
                textBox.TabIndex = 12;
                textBox.Enabled = false;
                textBox.Show();
                this.Controls.Add(textBox);
                dynamicTextBoxes.Add(textBox);

                currentY += 30;
            }
            this.ResumeLayout();
        }

        private int GetRowForCode(string code)
        {
            var range = usedRange.Find(
                What: code,
                LookIn: XlFindLookIn.xlValues,
                LookAt: XlLookAt.xlPart,
                SearchOrder: XlSearchOrder.xlByRows,
                SearchDirection: XlSearchDirection.xlNext,
                MatchCase: false);
            if(range == null)
            {
                return 0;
            }
            var address = range.get_Address(Microsoft.Office.Interop.Excel.XlReferenceStyle.xlR1C1);
            string[] splt = address.Split('$');
            return Convert.ToInt32(splt[2]);
        }
        private void IncrementAmount(int CodeRow)
        {
            var currentAmount = worksheet.Cells[CodeRow, amountColumnNumber].Value;
            worksheet.Cells[CodeRow, amountColumnNumber].Value = Convert.ToInt32(currentAmount.ToString()) + 1;
        }

        private void ColorRow(int CodeRow)
        {
            Range range = worksheet.Range[worksheet.Cells[CodeRow, 1], worksheet.Cells[CodeRow, dynamicLabels.Count + 1]];
            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
        }

        private int GetColumnIndex(ComboBox comboBox)
        {
            var itemIndex = comboBox.SelectedIndex;
            var itemValue = (string)comboBox.Items[itemIndex];
            for (int j = 1; j <= usedRange.Columns.Count; j++)
            {
                if(worksheet.Cells[1, j].Value == null)
                {
                    return 0;
                }
                string content = worksheet.Cells[1, j].Value.ToString();
                if(content.Equals(itemValue))
                {
                    return j;
                }
            }
            return 0;
        }

        private void CodeNotFound(string Barcode)
        {
            PlayFailureSound();
            for (int textBoxIndex = 0; textBoxIndex < dynamicTextBoxes.Count; textBoxIndex++)
            {
                Invoke(new System.Action(() => {
                    var tbCurrent = dynamicTextBoxes[textBoxIndex];
                    if (tbCurrent.Name == ("tb_" + cbBarcode.Text))
                    {
                        tbCurrent.Text = Barcode;
                        tbCurrent.Enabled = false;
                    }
                    else if (tbCurrent.Name == ("tb_" + cbAmount.Text))
                    {
                        tbCurrent.Text = "1";
                        tbCurrent.Enabled = false;
                    }
                    else
                    {
                        tbCurrent.Text = "";
                        tbCurrent.Enabled = true;
                    }

                    bAddProduct.Enabled = true;
                }));
            }
        }

        private void CodeFound(int CodeRow)
        {
            IncrementAmount(CodeRow);
            ColorRow(CodeRow);
            LoadDataInDynamicTextBox(CodeRow);
            workbook.Save();
            PlaySuccessSound();
        }

        private void PlaySuccessSound()
        {
            var sp = new SoundPlayer(@"sounds\on_scan_success.wav");
            sp.Play();
        }

        private void PlayFailureSound()
        {
            var sp = new SoundPlayer(@"sounds\on_scan_failure.wav");
            sp.Play();
        }

        private void LoadDataInDynamicTextBox(int codeRow)
        {
            for (int textBoxIndex = 0; textBoxIndex < dynamicTextBoxes.Count; textBoxIndex++)
            {
                Invoke(new System.Action(() => {
                    if (worksheet.Cells[codeRow, textBoxIndex + 1].Value == null)
                    {
                        dynamicTextBoxes[textBoxIndex].Text = "";
                    }
                    else
                    {
                        dynamicTextBoxes[textBoxIndex].Text = worksheet.Cells[codeRow, textBoxIndex + 1].Value.ToString();
                    }
                    dynamicTextBoxes[textBoxIndex].Enabled = false;
                    bAddProduct.Enabled = false;
                }));
            }
        }
        public void ProcessBarcode1(string ScannedCode)
        {
            MessageBox.Show(ScannedCode);
        }
        public void ProcessBarcode(string ScannedCode)
        {
            Invoke(new System.Action(() => { tbScanned.Text = ScannedCode; }));
            int codeRow = GetRowForCode(ScannedCode);
            if (codeRow == 0)
            {
                CodeNotFound(ScannedCode);
            }
            else
            {
                CodeFound(codeRow);
            }
        }
        public void Cleanup()
        {
            if (workbook != null)
            {
                workbook.Save();
                workbook.Close(true);
                workbook = null;
            }
        }
        public void StopExcel()
        {
            if (application != null)
            {
                application.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(application);
                application = null;
            }
        }

        public void ReinitData()
        {
            Cleanup();
            InitInventory();
        }
        #endregion Utils

        #region Callbacks
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Title = "Browse inventory table",
                CheckFileExists = true,
                CheckPathExists = true,
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                tbInventoryPath.Text = openFileDialog.FileName;
                InitInventory();
                PopulateListboxes();
                InitializeDynamicFields();
            }
        }
        private void cbBarcode_SelectedIndexChanged(object sender, EventArgs e)
        {
            codeColumnNumber = GetColumnIndex((ComboBox)sender);
            if (codeColumnNumber == 0)
            {
                MessageBox.Show("Column chosen for barcode not found");
                return;
            }
        }

        private void cbAmount_SelectedIndexChanged(object sender, EventArgs e)
        {
            amountColumnNumber = GetColumnIndex((ComboBox)sender);
            if(amountColumnNumber == 0)
            {
                MessageBox.Show("Column chosen for barcode not found");
                return;
            }
        }

        void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cleanup();
            StopExcel();
        }
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (worksheet == null)
            {
                MessageBox.Show("Open the inventory first");
                return;
            }
            var currentBarcode = port.ReadExisting().Trim();
            ProcessBarcode(currentBarcode);
        }

        private void bAddProduct_Click(object sender, EventArgs e)
        {
            var nextFreeRow = usedRange.Rows.Count + 1;
            for(int j = 0; j < dynamicTextBoxes.Count; j++)
            {
                worksheet.Cells[nextFreeRow, j + 1].Value = dynamicTextBoxes[j].Text;
            }
            ColorRow(nextFreeRow);
            workbook.Save();
            
            PlaySuccessSound();
            foreach(Control ctrl in dynamicTextBoxes)
            {
                ctrl.Enabled = false;
            }
            bAddProduct.Enabled = false;

            // need to reinitialize when adding a new product
            // otherwise later updates to the product will fail
            ReinitData();
        }

        private void bStartScanning_Click(object sender, EventArgs e)
        {
            StartListeningForStdin();
        }

        private void bExit_Click(object sender, EventArgs e)
        {
            if(this.thread != null)
            {
                this.thread.Abort();
            }
            this.Close();
            Environment.Exit(0);
        }
    }
#endregion Callbacks
}
