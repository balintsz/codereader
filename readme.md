# Summary

This is a barcode reader app. It's used to quickly count products in a store by scanning their barcodes.
As input, this app needs an inventory extract from an accounting application in the form of an excel file.
The app is generic, it can work with any excel file that contains a column for barcodes and a column for amount.
It is the job of the user to select which column contains the barcodes and which column contains the amount.
If a barcode is found, the app will increment it's amount and highlight with yellow the corresponding line in the excel file.
If a barcode is not found, the user can add the product to the inventory list.
In both cases, the app emits different sounds.

# Quick guide

## Open CodeReader.exe:

![alt text](docs/open_codereader.png)

## Open the inventory excel file:

![alt text](docs/open_inventory.png)

After opening the inventory excel file, the names of the columns will appear in the bottom part of the window.

## Choose the columns that contain barcodes and amounts

![alt text](docs/choose_columns.png)

## Start scanning

### In case the barcode is found

In case the barcode is found, the app will increment the column chosen as the amount column, in this case "stoc", and display the updated entry in the inventory. The corresponding row in the excel file is highlighted with yellow.
The excel file is saved automatically.

![alt text](docs/case_found.png)

### In case the barcode is not found

In case the barcode is not found, the text fields become editable.
The user completes the data for the new product entry and presses the "Add product" button.
Then, the product will be added to the excel file and it's corresponding row will be highlighted.
The excel file is saved automatically.

![alt text](docs/case_new_1.png)

## The excel file is automatically saved when the user closes the app window